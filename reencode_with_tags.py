import argparse
import datetime
import json
import os
import shlex
import shutil
import subprocess
import sys

from dateutil import parser as date_parser # not sure why this works:  https://github.com/gunthercox/ChatterBot/issues/1220#issuecomment-368787671
from pathlib import Path

FFMPEG_METADATA_DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"


def get_file_modified_date(video_path):
    try:
        last_modified = os.path.getmtime(str(video_path))
        return datetime.datetime.fromtimestamp(last_modified).strftime(FFMPEG_METADATA_DATETIME_FORMAT)
    except:
        return None


def get_date_from_format(json_out, tag):
    try:
        return json_out["format"]["tags"][tag]
    except:
        return None


def get_date_from_stream(json_out, tag):
    try:
        return json_out["streams"][0]["tags"][tag]
    except:
        return None


def get_date_from_folder_name(video_path):
    folder_name = video_path.parts[-2]
    for part in folder_name.split():
        if len(part) < 8: # yyyymmdd
            return None
        try:
            return date_parser.parse(part, fuzzy=True).strftime("%Y-%m-%d 00:00:00")
        except:
            if args.verbose:  print(sys.exc_info()[0])
            continue
    return None
    
    
def get_current_timestamp_metadata():
    return datetime.datetime.now().strftime(FFMPEG_METADATA_DATETIME_FORMAT)


def get_current_timestamp():
    return datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")


def sanitize_date(encode_date):
    try:
        parsed_date = date_parser.isoparse(encode_date)
        if None != parsed_date.tzinfo and None != parsed_date.tzinfo.utcoffset(parsed_date):
            return encode_date # UTC time, this date doesn't need sanitizing and is ready to go for ffmpeg
    except:
        pass
        
    KNOWN_DATE_FORMATS = [FFMPEG_METADATA_DATETIME_FORMAT, "%Y:%m:%d %H:%M:%S"]
    for date_format in KNOWN_DATE_FORMATS:
        try:
            parsed_date = datetime.datetime.strptime(encode_date, date_format) 
            return parsed_date.strftime(FFMPEG_METADATA_DATETIME_FORMAT)
        except ValueError as e:
            if args.verbose:  print(e)
            continue
    
    return None


def get_encode_date(ffprobe_path, video_path):
    # ffprobe known tags in stream section and format section
    KNOWN_TAGS = ["DateTime", "creation_time"]
    KNOWN_SECTIONS = [("format_tags", get_date_from_format), ("stream_tags", get_date_from_stream)]
    ffprobe_command_base = "{} -v quiet \"{}\" -print_format json -show_entries".format(ffprobe_path.as_posix(), video_path.as_posix())

    for section in KNOWN_SECTIONS:
        for tag in KNOWN_TAGS:
            ffprobe_command = "{} {}".format(ffprobe_command_base, section[0])
            if args.verbose:  print(ffprobe_command)
            proc = subprocess.Popen(shlex.split(ffprobe_command), stdout=subprocess.PIPE, universal_newlines=True)
            out, err = proc.communicate()
            if args.verbose:  print(out)
            
            ffprobe_output_json = json.loads(out)
            encode_date = section[1](ffprobe_output_json, tag)
            if None != encode_date:
                return sanitize_date(encode_date)

    return get_date_from_folder_name(video_path)


def get_create_mp4_outfile_path(infile_path):
    # construct path to output file, with .mp4 extension
    infile_dir = infile_path.parent
    outfile_path = infile_dir / "{}.{}".format(infile_path.stem, "mp4")

    return outfile_path


def move_to_backup_dir(input_file_path, scan_dir_path, backup_dir_path):
    source_path_end_parts = input_file_path.parts[len(scan_dir_path.parts):] # get everything after scan_dir_path
    backup_file_path = Path(backup_dir_path)
    for part in source_path_end_parts:
        backup_file_path = backup_file_path / part
    backup_file_path.parent.mkdir(parents=True, exist_ok=True)
    if backup_file_path.exists(): # probably shouldn't ever happen
        print("backup file path exists:  {}".format(str(backup_file_path)))
        return
    input_file_path.replace(backup_file_path)
        

# assumes input video lives in a subdirectory
def reencode_video_with_date(ffmpeg_path, video_path, encode_date, comment, outfile_path, stdout_f, stderr_f):
    ffmpeg_command = "{} -y -i \"{}\" -c:v h264_nvenc -preset slow -profile:v high -crf 18 -coder 1 -pix_fmt yuv420p -movflags +faststart -g 30 -bf 2 -c:a aac -b:a 384k -profile:a aac_low -strict -2 -metadata date=\"{}\" -metadata comment=\"{}\" \"{}\"".format(ffmpeg_path.as_posix(), video_path.as_posix(), encode_date, comment, outfile_path.as_posix())
    print(ffmpeg_command)
    subprocess.run(shlex.split(ffmpeg_command), stdout=stdout_f, stderr=stderr_f)


parser = argparse.ArgumentParser()
parser.add_argument('--indir', '-i', required = True, help = "path to scan")
parser.add_argument('--backupdir', '-b', required = True, help = "path where original source files will be moved")
parser.add_argument('--ffmpeg', '-m', required = True, help = "full path to ffmpeg executable")
parser.add_argument('--ffprobe', '-p', required = True, help = "full path to ffprobe executable")
parser.add_argument('--verbose', '-v', required = False, action='store_true', help = "print more stuff")

args = parser.parse_args()

# collect avi and mov files
paths = list(Path(args.indir).rglob("*.[aA][vV][iI]"))
paths.extend(list(Path(args.indir).rglob("*.[mM][oO][vV]")))
paths.extend(list(Path(args.indir).rglob("*.[wW][mM][vV]")))
paths.extend(list(Path(args.indir).rglob("*.[fF][lL][vV]")))
paths.extend(list(Path(args.indir).rglob("*.[mM][pP][eE][gG]")))

# log files
now = get_current_timestamp()
stdout_file_path = Path(args.indir) / "{}_out.log".format(now)
stderr_file_path = Path(args.indir) / "{}_err.log".format(now)

no_date = []
with stdout_file_path.open('w+') as stdout_f, stderr_file_path.open('w+') as stderr_f:
    for path in paths:
        print(path)
        input_file_path = Path(path)
        encode_date = get_encode_date(Path(args.ffprobe), input_file_path)
        outfile_path = get_create_mp4_outfile_path(input_file_path)
        if None == encode_date:
            no_date.append(path)
            encode_date = get_file_modified_date(input_file_path)
            if None == encode_date:
                encode_date = get_current_timestamp_metadata()
        print(encode_date)
        reencode_video_with_date(Path(args.ffmpeg), input_file_path, encode_date, input_file_path.parts[-2], outfile_path, stdout_f, stderr_f)
        move_to_backup_dir(input_file_path, Path(args.indir), Path(args.backupdir))

for dateless in no_date:
    print(dateless)

    # make a copy
    #outdir_path = Path(args.outdir) / "dateless" / Path(dateless).parts[-2]
    #outdir_path.mkdir(parents=True, exist_ok=True)
    #shutil.copy2(Path(dateless).as_posix(), outdir_path.as_posix())
