from pathlib import Path

import argparse
import re
import sys


argparser = argparse.ArgumentParser()
argparser.add_argument('-i', '--input', required=True)
args = argparser.parse_args()

journalpath = Path(args.input)
if not journalpath.is_file():
    sys.exit(f"Invalid input path:  {journalpath.absolute()}")


contents = journalpath.read_text()
p = re.compile(r"^\d{1,2}:\d{2}.*[\r\n|\r|\n]^.+day, .+ \d{1,2}, \d{4} \(.+\)[\r\n|\r|\n]^Time in .+[\r\n|\r|\n]", re.MULTILINE) # beware of potential BOM at start of file
start = 0
spans = []
while True:
    m = p.search(contents, start)
    if m is None:
        break
    spans.append(m.span())
    start = m.end()

chunks = []
for i in range(len(spans) - 1):
    date_start = spans[i][0]
    date_end = spans[i][1] - 1
    content_start = spans[i][1]
    content_end = spans[i+1][0] - 1

    chunks.append((contents[date_start:date_end], contents[content_start:content_end]))

#    print("date start")
#    print(contents[date_start:date_end])
#    print("date end")
#    print("chunk start")
#    print(contents[content_start:content_end])
#    print("chunk end")
last = spans[-1]
chunks.append((contents[last[0]:last[1] - 1], contents[last[1]:]))
