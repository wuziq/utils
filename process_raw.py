import argparse
import shlex
import subprocess
import sys

from pathlib import Path


argparser = argparse.ArgumentParser()
argparser.add_argument('-i', '--input-dir', dest='input_dir', required=True)
argparser.add_argument('-o', '--output-dir', dest='output_dir', required=True)
argparser.add_argument('-p', '--pp3-path', dest='pp3_path', required=True)
argparser.add_argument('-r', '--rt-path', dest='rt_path', required=False, default="c:\\Program Files\\RawTherapee\\5.8\\rawtherapee-cli.exe")
argparser.add_argument('-y', '--overwrite', action='store_true', required=False, default=False)
args = argparser.parse_args()

rt_path = Path(args.rt_path)
if not rt_path.is_file():
    sys.exit(f"Invalid rawtherapee-cli:  {rt_path.absolute()}")
pp3_path = Path(args.pp3_path)
if not pp3_path.is_file():
    sys.exit(f"Invalid .pp3 file:  {pp3_path.absolute()}")
input_dir = Path(args.input_dir)
if not input_dir.is_dir():
    sys.exit(f"Invalid input dir:  {input_dir.absolute()}")
output_dir = Path(args.output_dir)
if output_dir.is_dir() and not args.overwrite:
    sys.exit(f"Output dir already exists and overwrite isn't enabled:  {output_dir.absolute()}")

output_dir.mkdir(exist_ok=True, parents=True)
rt_cmd = f"{rt_path.absolute().as_posix()} -o {output_dir.absolute().as_posix()} -q -p {pp3_path.absolute().as_posix()} -t -c {input_dir.absolute().as_posix()}"
proc = subprocess.Popen(shlex.split(rt_cmd), stdout=sys.stdout, stderr=sys.stderr, universal_newlines=True)
proc.communicate()
