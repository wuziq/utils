from gphotospy import authorize
from gphotospy.media import Media
from gphotospy.media import MEDIAFILTER

import json

# Select secrets file
CLIENT_SECRET_FILE = "gphoto_oauth.json"

# Get authorization and return a service object
service = authorize.init(CLIENT_SECRET_FILE)

# Init the media manager
media_manager = Media(service)

# Set default behaviors (don't show archived media)
media_manager.show_archived(True)
media_manager.set_search_pagination(100)
media_manager.show_only_created(False)

list_iterator = media_manager.list()
list_metadata = {}
for media in list_iterator:
    if "video" not in media.get("mimeType"):
        continue
    filename = media.get("filename")
    print(filename)
    metadata = media.get("mediaMetadata")
    metadata_dict = {"creationTime": metadata.get("creationTime"), "width": metadata.get("width"),
                     "height": metadata.get("height")}
    list_metadata.setdefault(filename, list()).append(metadata_dict)
with open("list_metadata.json", "w") as f:
    json.dump(obj=list_metadata, fp=f, sort_keys=True, indent=4)

# THIS IS BROKEN.  for some reason, search only ever returns the same 75 results over and over, and the iterator never thinks it's done.
# Search by media type
# search_iterator = media_manager.search(filter=[MEDIAFILTER.VIDEO])
# video_metadata = {}
# for media in search_iterator:
#     filename = media.get("filename")
#     print(filename)
#     metadata = media.get("mediaMetadata")
#     metadata_dict = {"creationTime": metadata.get("creationTime"), "width": metadata.get("width"),
#                      "height": metadata.get("height")}
#     video_metadata.setdefault(filename, list()).append(metadata_dict)
# with open("video_metadata.json", "w") as f:
#     json.dump(obj=video_metadata, fp=f, sort_keys=True, indent=4)

