import argparse
import json
import shlex
import subprocess

from dateutil import parser as date_parser
from pathlib import Path


def get_metadata_date(json_out):
    for tag in ["date", "creation_time"]:
        try:
            date = json_out["format"]["tags"][tag]
            return date_parser.parse(date).strftime('%Y-%m-%dT%H:%M:%SZ')
        except:
            continue

    print("WARNING:  No date or creation_time found in ffprobe output:")
    print(json_out)
    return None


def get_metadata_width(json_out):
    for index in [0, 1]:
        try:
            return str(json_out["streams"][index]["width"])
        except:
            continue

    print("WARNING:  No width found in ffprobe output:")
    print(json_out)
    return None


def get_metadata_height(json_out):
    for index in [0, 1]:
        try:
            return str(json_out["streams"][index]["height"])
        except:
            continue

    print("WARNING:  No height found in ffprobe output:")
    print(json_out)
    return None


def get_metadata(ffprobe_path, possible_dupe_path):
    ffprobe_command = "{} -v quiet \"{}\" -print_format json -show_streams -show_format".format(ffprobe_path.as_posix(), possible_dupe_path.as_posix())
    proc = subprocess.Popen(shlex.split(ffprobe_command), stdout=subprocess.PIPE, universal_newlines=True)
    out, err = proc.communicate()

    ffprobe_output_json = json.loads(out)
    metadata = {}
    metadata["date"] = get_metadata_date(ffprobe_output_json)
    metadata["width"] = get_metadata_width(ffprobe_output_json)
    metadata["height"] = get_metadata_height(ffprobe_output_json)
    return metadata


def move_to_dupe_dir(dupe_dir_path, file_path, scan_dir_path):
    source_path_end_parts = file_path.parts[len(scan_dir_path.parts):]  # get everything after scan_dir_path
    dupe_file_path = dupe_dir_path
    for part in source_path_end_parts:
        dupe_file_path = dupe_file_path / part
    dupe_file_path.parent.mkdir(parents=True, exist_ok=True)
    if dupe_file_path.exists():
        print("WARNING:  dupe file already exists in dupe dir:  {}".format(str(dupe_file_path)))
        return
    print("Moving duplicate from {} to {}".format(str(file_path), str(dupe_file_path)))
    file_path.replace(dupe_file_path)


arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('-i', '--in-dir', dest='in_dir', required=True)
arg_parser.add_argument('-d', '--dupe-dir', dest='dupe_dir', required=True)
arg_parser.add_argument('-p', '--ffprobe', required=True)
arg_parser.add_argument('-e', '--existing', required=True)
args = arg_parser.parse_args()

possible_dupe_paths = list(Path(args.in_dir).rglob("*.[mM][pP][4]"))
print("Found {} possible dupes.".format(len(possible_dupe_paths)))

existing = None
with open(args.existing) as f:
    existing = json.load(f)
print("Checking against {} existing videos...".format(len(existing)))

for possible_dupe_path in possible_dupe_paths:
    if possible_dupe_path.name in existing:
        metadata = get_metadata(Path(args.ffprobe), possible_dupe_path)
        existing_metadata_list = existing[possible_dupe_path.name]
        for existing_metadata in existing_metadata_list:
            if metadata["date"] == existing_metadata["creationTime"] and \
               metadata["width"] == existing_metadata["width"] and \
               metadata["height"] == existing_metadata["height"]:
                move_to_dupe_dir(Path(args.dupe_dir), possible_dupe_path, Path(args.in_dir))
                break
