import argparse
import shlex
import subprocess
import sys

from pathlib import Path


def get_start_number(input_dir: Path) -> int:
    first = sorted(input_dir.glob("*.tif"))[0]
    return int(first.stem.lstrip("DSC0"))


def get_scale(vertical: bool) -> str:
    if vertical:
        return "2564:3840,crop=2160:3840:404:0"
    else:
        return "3840:2160:force_original_aspect_ratio=decrease,pad=3840:2160:(ow-iw)/2:(oh-ih)/2,setsar=1"


def get_watermark_coords(vertical: bool) -> str:
    if vertical:
        return "x=W-tw-80:y=H-th-80"
    else:
        return "x=3000:y=H-th-80"


def get_font_path(font_path: Path) -> str:
    path = font_path.absolute().as_posix()
    return path[path.find(':') + 1:]


argparser = argparse.ArgumentParser()
argparser.add_argument('-i', '--input-dir', dest='input_dir', required=True)
argparser.add_argument('-o', '--output-dir', dest='output_dir', required=False, default="C:\\Users\\wuziq\\Videos")
argparser.add_argument('-f', '--ffmpeg-path', dest='ffmpeg_path', required=False, default="c:\\Users\\wuziq\\ffmpeg\\bin\\ffmpeg.exe")
argparser.add_argument('-t', '--font-path', dest='font_path', required=False, default="C:\\Users\\wuziq\\Videos\\fonts\\opensans-300.ttf")
argparser.add_argument('-v', '--vertical', action='store_true', required=False, default=False)
argparser.add_argument('-y', '--overwrite', action='store_true', required=False, default=False)
argparser.add_argument('-r', '--framerate', required=False, default=24)
args = argparser.parse_args()

ffmpeg_path = Path(args.ffmpeg_path)
if not ffmpeg_path.is_file():
    sys.exit(f"Invalid ffmpeg:  {ffmpeg_path.absolute()}")
font_path = Path(args.font_path)
if not font_path.is_file():
    sys.exit(f"Invalid font file:  {font_path.absolute()}")
input_dir = Path(args.input_dir)
if not input_dir.is_dir():
    sys.exit(f"Invalid input dir:  {input_dir.absolute()}")
output_dir = Path(args.output_dir)
if not output_dir.is_dir():
    sys.exit(f"Invalid output dir:  {output_dir.absolute()}")
output_path = output_dir / f"{input_dir.parts[-1]}_watermarked.mp4"
if output_path.is_file() and not args.overwrite:
    sys.exit(f"File already exists (use -y to overwrite):  {output_path.absolute()}")

output_path.parent.mkdir(exist_ok=True, parents=True)
ffmpeg_cmd = f"{ffmpeg_path.absolute().as_posix()} {'-y' if args.overwrite else ''} -vsync 0 -r {args.framerate} -f image2 -start_number {get_start_number(input_dir)} -i {input_dir.absolute().as_posix()}/DSC%05d.tif -vf \"scale={get_scale(args.vertical)},drawtext=fontfile={get_font_path(font_path)}:text='youtube.com/wuziq':fontsize=48:fontcolor=0x5a5a5a:{get_watermark_coords(args.vertical)}\" -sws_flags lanczos+full_chroma_inp+accurate_rnd+full_chroma_int -vframes 9999 -c:v h264_nvenc -preset slow -profile:v high -cq:v 16 -coder 1 -pix_fmt yuv420p -movflags +faststart -g {args.framerate} -bf 2 -rc-lookahead 20 {output_path.absolute().as_posix()}"
print(ffmpeg_cmd)
proc = subprocess.Popen(shlex.split(ffmpeg_cmd), stdout=sys.stdout, stderr=sys.stderr, universal_newlines=True)
proc.communicate()
